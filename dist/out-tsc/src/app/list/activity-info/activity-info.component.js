var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { EntityService } from '../services/entity.service';
import { Network } from '@ionic-native/network/ngx';
var ActivityInfoComponent = /** @class */ (function () {
    function ActivityInfoComponent(entityService, network) {
        this.entityService = entityService;
        this.network = network;
        this.isLoading = false;
    }
    ActivityInfoComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.entityService.loading.subscribe(function (value) {
            _this.isLoading = value;
            _this.error = null;
        });
        this.entityService.error.subscribe(function (value) {
            if (value) {
                _this.error = value;
                _this.isLoading = false;
            }
        });
        this.network.onDisconnect().subscribe(function () {
            _this.isLoading = false;
            _this.error = 'Offline';
        });
        this.network.onConnect().subscribe(function () {
            console.log('sadsadsa');
            _this.error = null;
            _this.retry();
        });
    };
    ActivityInfoComponent.prototype.retry = function () {
        this.entityService.getData(1).subscribe();
    };
    ActivityInfoComponent = __decorate([
        Component({
            selector: 'app-activity-info',
            templateUrl: './activity-info.component.html',
            styleUrls: ['./activity-info.component.scss']
        }),
        __metadata("design:paramtypes", [EntityService,
            Network])
    ], ActivityInfoComponent);
    return ActivityInfoComponent;
}());
export { ActivityInfoComponent };
//# sourceMappingURL=activity-info.component.js.map