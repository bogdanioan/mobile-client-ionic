import { environment } from 'src/environments/environment';
var api = environment.host;
export var urls = {
    login: (function () { return [api, 'auth', 'login'].join('/'); })(),
    dataURL: (function () { return [api, 'recipes'].join('/'); })()
};
//# sourceMappingURL=constants.js.map