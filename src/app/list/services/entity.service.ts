import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { urls } from 'src/app/shared/constants';
import { tap } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EntityService {
  entities = new BehaviorSubject<any>(null);
  masterDetailEntity = new BehaviorSubject<any>(null);
  loading = new BehaviorSubject<boolean>(false);
  error = new BehaviorSubject<string>(null);
  private ws: WebSocket;
  token: string;
  lastPageRequested = 0;
  addEntity = new BehaviorSubject<any>(null);
  editEntity = new BehaviorSubject<any>(null);
  deleteEntity = new BehaviorSubject<any>(null);

  constructor(private http: HttpClient,
    private storage: Storage
  ) {
    this.storage.get('token').then(
      token => {
        this.token = token;
        this.listenChanges();
      });
  }

  getData(page) {
    this.loading.next(true);
    let ifModifiedSince;
    this.storage.get(`page${page}`).then(dataOffline => {
      if (dataOffline) {
        this.entities.next(dataOffline);
        ifModifiedSince = dataOffline.lastModified;
      }
      let headers;
      if (ifModifiedSince) {
        headers = { 'If-Modified-Since': ifModifiedSince };
      }
      this.http.get(`${urls.dataURL}?page=${page}`, { observe: 'response', headers }).subscribe(
        (resp: any) => {
          const lastModified = resp.headers.get('Last-Modified');
          const data = resp.body;
          this.entities.next(data);
          this.loading.next(false);

          this.storage.set(`page${page}`, { lastModified, ...data });
          this.lastPageRequested = page;
        },
        err => {
          if (err.status !== 304) {
            this.error.next(err.message);
          } else {
            this.loading.next(false);
          }
        }
      );

    });
  }

  setMasterDetail(entity) {
    this.masterDetailEntity.next(entity);
  }

  listenChanges() {
    // Let us open a web socket
    this.ws = new WebSocket(environment.ws);

    this.ws.onopen = () => {
      // Web Socket is connected, send data using send()
      // protocol web socket
      // this.ws.send("Message to send");
      console.log('Connected...');
      this.ws.send(this.token);
    };

    this.ws.onclose = () => {
      // websocket is closed.
      console.log('Connection closed...');
    };


    this.ws.onmessage = (evt) => {
      const data = JSON.parse(evt.data);
      const eventType = environment.eventType;
      switch (data.event) {
        case eventType.add: {
          this.addEntity.next(data.payload);
          break;
        }
        case eventType.delete: {
          console.log('aici');
          this.deleteEntity.next(data.payload);
          break;
        }
        case eventType.update: {

          break;
        }
        default: {
          break;
        }
      }
    };
  }

  delete(id: any) {
    return this.http.delete(`${urls.dataURL}/${id}`);
  }

  unlistenChanges() {
    this.ws.close();
  }
}
