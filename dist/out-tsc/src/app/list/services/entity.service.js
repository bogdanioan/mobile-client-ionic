var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { urls } from 'src/app/shared/constants';
import { tap } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
var EntityService = /** @class */ (function () {
    function EntityService(http, storage) {
        this.http = http;
        this.storage = storage;
        this.entities = new BehaviorSubject(null);
        this.loading = new BehaviorSubject(false);
        this.error = new BehaviorSubject(null);
    }
    EntityService.prototype.getData = function (page) {
        var _this = this;
        this.loading.next(true);
        this.storage.get("page" + page).then(function (data) {
            if (data) {
                data = JSON.parse(data);
                _this.entities.next(data);
            }
        });
        return this.http.get(urls.dataURL + "?page=" + page).pipe(tap(function (data) {
            _this.entities.next(data);
            _this.loading.next(false);
            _this.storage.set("page" + page, JSON.stringify(data));
        }, function (err) { return _this.error.next(err.message); }));
    };
    EntityService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [HttpClient,
            Storage])
    ], EntityService);
    return EntityService;
}());
export { EntityService };
//# sourceMappingURL=entity.service.js.map