import { Component, OnInit } from '@angular/core';
import { EntityService } from '../services/entity.service';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  entities = [];
  more = true;
  constructor(private entityService: EntityService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }
  primaryKey = environment.entityPrimaryKey;

  ngOnInit() {
    this.entityService.entities.subscribe((data: any) => {
      if (!data) {
        return;
      }
      if (this.entities.length < data.page) {
        this.entities.push(data[environment.entityName]);
        this.more = data.more;
      } else {
        this.entities.splice(data.page - 1, 1, data[environment.entityName]);
      }
    });
    this.entityService.addEntity.subscribe((data: any) => {
      if (data) {
        if (this.entities.length > 0) {
          this.entities[0].splice(0, 0, data);
        } else {
          this.entities.push([data]);
        }
      }
      console.log(this.entities);
    });
    this.entityService.editEntity.subscribe((data: any) => {
      console.log(this.entities);
    });
    this.entityService.deleteEntity.subscribe((data: any) => {
      if (data) {
        console.log('si aici');
        for (const page of this.entities) {
          const pageLength = page.length;
          for (let i = 0; i < pageLength; i++) {
            const item = page[i];
            if (item[this.primaryKey] === data) {
              page.splice(i, 1);
              console.log(this.entities);
              return;
            }
          }
        }
      }
    });
    this.loadData();
  }

  loadData(event?) {
    if (this.more) {
      this.entityService.getData(this.entities.length + 1);
      if (event) {
        event.target.complete();
      }
    } else {
      event.target.complete();
    }
  }

  masterDetail(entity) {
    console.log(entity);
    this.entityService.setMasterDetail(entity);
    this.router.navigate(['entity'], { relativeTo: this.activatedRoute });
  }
}
