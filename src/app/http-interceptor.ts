import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

import { Observable, throwError, from } from 'rxjs';
import { catchError, mergeMap } from 'rxjs/operators';
@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    constructor(private storage: Storage) { }

    // Intercepts all HTTP requests!
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const promise = this.storage.get('token');

        return from(promise).pipe(
            mergeMap(token => {
            const clonedReq = this.addToken(request, token);
            return next.handle(clonedReq).pipe(
                catchError(error => {
                    // Perhaps display an error for specific status codes here already?
                    // Pass the error to the caller of the function
                    return throwError(error);
                })
            );
        }));
    }

    // Adds the token to your headers if it exists
    private addToken(request: HttpRequest<any>, token: any) {
        if (token) {
            let clone: HttpRequest<any>;
            clone = request.clone({
                setHeaders: {
                    Accept: `application/json`,
                    'Content-Type': `application/json`,
                    Authorization: `Bearer ${token}`
                }
            });
            return clone;
        }

        return request;
    }
}
