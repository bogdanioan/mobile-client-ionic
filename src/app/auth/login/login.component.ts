import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  constructor(private authService: AuthService,
    private storage: Storage,
    private router: Router
  ) { }

  ngOnInit() {
    this.storage.get('token').then(token => {
      console.log('aici');
      if (token) {
        this.navigateToHome();
      }
    });
  }

  login(email, password) {
    this.authService.login(email, password).subscribe(
      _ => this.navigateToHome(),
      err => console.log(err)
    );
  }

  navigateToHome() {
    this.router.navigate(['/list']);
  }

}
