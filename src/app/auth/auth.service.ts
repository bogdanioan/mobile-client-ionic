import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { urls } from '../shared/constants';
import { Storage } from '@ionic/storage';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient,
    private storage: Storage
  ) { }

  login(email, password) {
    console.log(urls.login);
    return this.http.post(urls.login, { email, password }).pipe(
      tap((token: any) => this.storage.set('token', token.token))
    );
  }
}
