import { Component, OnInit } from '@angular/core';
import { EntityService } from '../services/entity.service';
import { Network } from '@ionic-native/network/ngx';
import { of } from 'rxjs';
@Component({
  selector: 'app-activity-info',
  templateUrl: './activity-info.component.html',
  styleUrls: ['./activity-info.component.scss']
})
export class ActivityInfoComponent implements OnInit {
  isLoading = false;
  error;
  constructor(private entityService: EntityService,
    private network: Network
  ) { }

  ngOnInit() {
    this.entityService.loading.subscribe(value => {
      this.isLoading = value;
      this.error = null;
    });
    this.entityService.error.subscribe(value => {
      if (value) {
        if (this.error !== 'Offline') {
          this.error = `${value}... TAP TO RETRY`;
        }
        this.isLoading = false;
      }
    });

    this.network.onDisconnect().subscribe(() => {
      this.isLoading = false;
      this.error = 'Offline';
    });


    this.network.onConnect().subscribe(() => {
      this.error = null;
      this.retry();
    });
  }

  retry() {
    if (this.error !== 'Offline') {
      this.entityService.getData(1);
    }
  }

}
