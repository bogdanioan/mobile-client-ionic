import { Component, OnInit, OnDestroy } from '@angular/core';
import { EntityService } from '../services/entity.service';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ModalController } from '@ionic/angular';
import { LoadingIndicatorComponent } from '../loading-indicator/loading-indicator.component';

@Component({
  selector: 'app-master-detail',
  templateUrl: './master-detail.component.html',
  styleUrls: ['./master-detail.component.scss']
})
export class MasterDetailComponent implements OnInit, OnDestroy {
  entity;
  subscriptions: Subscription[] = [];
  constructor(private entityService: EntityService,
    private router: Router,
    private modalController: ModalController
  ) { }

  ngOnInit() {
    this.subscriptions.push(this.entityService.masterDetailEntity.subscribe(entity => this.entity = entity));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  typeOf(value) {
    if (typeof value === 'string') {
      return 'text';
    }
    return typeof value;
  }
  delete() {
    this.modalController.create({
      component: LoadingIndicatorComponent
    });
    const id = this.entity[environment.entityPrimaryKey];
    this.subscriptions.push(this.entityService.delete(id).subscribe(_ => {
      this.modalController.dismiss();
      this.router.navigate(['/list']);
    }));
  }
  cancel() {
    this.router.navigate(['/list']);
  }
}
