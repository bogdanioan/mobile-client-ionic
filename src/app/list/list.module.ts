import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { EntityService } from './services/entity.service';
import { ListRoutingModule } from './list-routing.module';
import { IonicModule } from '@ionic/angular';
import { ActivityInfoComponent } from './activity-info/activity-info.component';
import { MasterDetailComponent } from './master-detail/master-detail.component';
import { LoadingIndicatorComponent } from './loading-indicator/loading-indicator.component';

@NgModule({
  declarations: [ListComponent, ActivityInfoComponent, MasterDetailComponent, LoadingIndicatorComponent],
  imports: [
    CommonModule, ListRoutingModule, IonicModule.forRoot()
  ],
  providers: [EntityService]
})
export class ListModule { }
