import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './list/list.component';
import { MasterDetailComponent } from './master-detail/master-detail.component';

const routes: Routes = [
    { path: '', component: ListComponent },
    { path: 'entity', component: MasterDetailComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ListRoutingModule { }
