import { environment } from 'src/environments/environment';

const api = environment.host;
export const urls = {
    login: (() => [api, 'auth', 'login'].join('/'))(),
    dataURL: (() => [api, environment.entityName].join('/'))()
};
